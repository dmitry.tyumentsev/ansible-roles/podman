Podman role
=========

Installs [Podman](https://podman.io/) on RHEL/CentOS, Debian/Ubuntu or AltLinux servers.

Example Playbook
----------------
```yml
- hosts: podman
  become: true
  roles:
    - podman
```
